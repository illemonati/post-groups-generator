
export type GroupElement = Array<number>;
export type Group = Array<GroupElement>

export const generateGroup = (length: number, group : Array<Array<number>> = []) => {
    const numbers = [...Array(length).keys()];
    // console.log(generatePermutations(numbers, 0, numbers.length-1));
    return generatePermutations(numbers, 0, numbers.length-1);
};

function* generatePermutations(arr: Array<any>, l: number, r: number) : Generator<Array<number>>{
    if (l === r) {
        yield arr.slice();
    } else {
        for (let i = l; i <= r; i++) {
            [arr[l], arr[i]] = [arr[i], arr[l]];
            yield * generatePermutations(arr, l + 1, r);
            [arr[l], arr[i]] = [arr[i], arr[l]];
        }
    }
}

export const createIdentityElement = (length: number) => {
    const res = new Array<number>(length);
    for (let i = 0; i < length; i++) {
        res[i] = i;
    }
    return res;
};

export const snap = (e1: GroupElement, e2: GroupElement): GroupElement => {
    if (e1.length !== e2.length) throw new Error("Only elements with equal length allowed!");
    const result: GroupElement = new Array<number>(e1.length);
    for (let i = 0; i < result.length; i++) {
        result[i] = e1[e2[i]];
    }
    return result;
};

export const findSnapPeriod = (e: GroupElement): number => {
    // debugger;
    let k = e;
    const i = JSON.stringify(createIdentityElement(e.length));
    let count = 1;
    while (JSON.stringify(k) !== i) {
        count++;
        k = snap(e, k);
    }
    return count;
};



