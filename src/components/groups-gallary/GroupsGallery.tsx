import React, {useEffect, useState} from "react";
import {findSnapPeriod, generateGroup} from "../../utils/group-utils";
import './styles.css';
import * as math from "mathjs";
interface GroupsGalleryProps {
    posts: number
}


class PostElement {
    name: string;
    img: string;
    period: number;
    constructor(name: string, img: string, period?: number) {
        this.name = name;
        this.img = img;
        this.period = period || 0;
    }
}

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


const GroupsGallery = (props: GroupsGalleryProps) => {

    const [postGroup, setPostGroup] = useState([new PostElement('', '', 0)]);


    const [enablePeriodCalculations, setEnablePeriodCalculations] = useState(true);

    const generatePostGroup = async () : Promise<undefined> => {

        const canvas = document.createElement('canvas') as HTMLCanvasElement;
        const ctx = canvas.getContext('2d') as CanvasRenderingContext2D;
        const postGap = 20;
        const postSize = 5;
        canvas.width = (postGap) * (props.posts + 1);
        canvas.height = canvas.width * 0.8;
        ctx.fillStyle = '#000000';

        let pg = postGroup.slice();
        while (pg.length > 0) {
            pg.pop();
        }
        setPostGroup(pg);

        // setPostGroup((ps) => {return new Array<PostElement>().slice()});

        if (props.posts < 1 || isNaN(props.posts) || !isFinite(props.posts)) {
            return;
        }
        setPostGroup(new Array<PostElement>());
        // setPostGroup([].slice());
        const permutations = generateGroup(props.posts);
        // console.log(permutations.length);
        const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        const numberOfPermutations = math.factorial(props.posts);

        for (let i = 0; i < numberOfPermutations; i++) {
            const permutation: Array<number> = permutations.next().value;
            // console.log(permutation);
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.lineWidth = postSize;
            ctx.beginPath();
            for (let i = 0; i < canvas.width; i ++) {
                ctx.moveTo((i+1) * postGap, 0.2*canvas.height);
                ctx.lineTo((permutation[i] + 1) * postGap, 0.8*canvas.height );
            }
            ctx.stroke();
            ctx.closePath();
            const img = canvas.toDataURL('image/png');
            let name = '';
            if (i !== 0) {
                let n = i - 1;
                const k = n % 26;
                // while (n >= 0) {
                //     name += letters[(n%26)];
                //     n -= 26;
                // }
                name += letters[k];
                name += (n - k) / 26;
            } else {
                name = 'I';
            }
            const pe = new PostElement(name, img, enablePeriodCalculations ? findSnapPeriod(permutation) : 0);
            const pg = postGroup;
            // console.log(postGroup);
            pg[i] = pe;
            setPostGroup(pg.slice());


            await sleep(0);
        }

        pg = postGroup.slice();
        while (pg.length > numberOfPermutations) {
            pg.pop();
        }
        setPostGroup(pg);

        return;
    };


    useEffect(() => {
        generatePostGroup().then();
        // eslint-disable-next-line
    }, [props.posts]    );


    return (
        <div className="GroupsGallery">
            <p>total amount = {props.posts}! = {postGroup.length}</p>
            <input name='enablePeriodCalculations' type='checkbox' checked={enablePeriodCalculations} onChange={(e) => setEnablePeriodCalculations(e.currentTarget.checked)}/>
            <label htmlFor="enablePeriodCalculations">enable period calculations (uncheck for speed)</label>
            <div className="grid-container">
                {postGroup.map((postElement, i) => {

                    return (
                        <div className="grid-item" key={i}>
                            <img src={postElement.img} alt="" className="grid-img"/>
                            <p className="post-element-name">{postElement.name}</p>
                            {(postElement.period !== 0 &&
                                <p className="post-element-period">Period: {postElement.period}</p>
                            )}
                        </div>
                    )
                })}
            </div>
        </div>
    )
};

export default GroupsGallery;
