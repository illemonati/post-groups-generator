import React, {useEffect, useState} from "react";
import GroupsGallery from "../groups-gallary/GroupsGallery";

const MainPage = () => {

    const [numberOfPosts, setNumberOfPosts] = useState(3);
    const [mode, setMode] = useState('groups');

    const changeNumberOfPosts = (value: string) => {
        const nVal = parseInt(value);
        setNumberOfPosts(nVal);
    };


    // eslint-disable-next-line
    const inverseMode = () => {
        setMode((mode === 'groups' ? 'matrix' : 'groups'));
    };

    useEffect(() => {

    }, [mode]);

    return (
        <div className="MainPage">
            <h1>Post Groups Generator</h1>
            <br />
            <label>Number Of Posts (Positive Integers Only): &nbsp; </label>
            <input name="number of posts" type="number" min="1" step="1" value={numberOfPosts} onChange={(e) => changeNumberOfPosts(e.currentTarget.value)}/>
            <br />
            <p>(highly recommend you stick to low numbers like 3 or 4)</p>
            <p>(anything over 6 takes too long)</p>
            <br />
            <br />
            {/*Will not implement*/}
            {/*<button onClick={(e) => {inverseMode();}}>{mode}</button>*/}
            {/*<br />*/}
            {/*<br />*/}
            {(mode === 'groups' ? <GroupsGallery posts={numberOfPosts}/> : <h1>Not Yet Implemented</h1>)}
        </div>
    )

};


export default MainPage;